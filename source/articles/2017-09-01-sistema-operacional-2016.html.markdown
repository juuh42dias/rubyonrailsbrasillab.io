---
title: Sistema Operacional mais usado para desenvolver Rails em 2016
date: 2017-09-01
tags: Comunidade
layout: post
author: brodock
---

Esse é o primeiro post da série (atrasada) de resultados das nossas pesquisas oficiais de opinião.
Queremos trazer um pouco mais de informação sobre o que a comunidade está utilizando, orientar quem está
querendo saber as melhroes práticas e provocar a discussão daqueles que se interessem em experimentar uma
outra opção.

<div data-figure="chart" data-focus="chart" data-series="rows"></div>

| Sistema Operacional | Pessoas |
| ------------------- | -------:|
| Linux               |	313     |
| Mac OS X            | 223     |
| Windows             | 6       |
| Outros              | 2       |

No caso do Windows, foram poucas amostras e divididas igualmente entre Windows 7 e Windows 10.

Em 2016 ainda não existia o projeto do [Windows Subsystem for Linux](https://msdn.microsoft.com/en-us/commandline/wsl/install_guide) que permite instalar Ubuntu, OpenSUSE ou SLES
em ambiente texto. Entenda que esse projeto não se trata de máquina virtual, e sim de emulação do kernel.
Ele funciona da mesma forma que o projeto do Wine, só que ao contrário, é o windows rodando nativamente
aplicações do Linux.

Se formos explorar as distribuições linux temos os seguintes dados:

<div data-figure="chart" data-focus="chart" data-series="rows"></div>

| Distribuição Linux | Pessoas |
| ------------------ | ------: |
| Ubuntu             | 234     |
| Mint               | 20      |
| Arch linux         | 16      |
| Fedora             | 11      |
| Elementary         | 7       |
| Debian             | 10      |
| Manjaro Linux      | 4       |
| Xubuntu            | 4       |
| Kubuntu            | 3       |
| Open Suse          | 1       |
| Lubuntu            | 1       |
| Slackware          | 1       |
| Gentoo             | 1       |

Tivemos também alguns trolls respondendo Windows 95 e Windows 3.11 :-)

Deixe aqui embaixo nos comentários se você acredita que teremos resultado diferente em 2017, ou se acredita
que o seu sistema operacional ou distribuição é a melhor pra desenvolver Rails e não encontra-se listada aqui.
